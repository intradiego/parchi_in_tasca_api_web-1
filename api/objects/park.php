<?php
class Park{
 
    // database connection and table name
    private $conn;
 
    // object properties
    public $id;   
    public $id_parco;
    public $id_categoria;
    public $name;
 
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }


    // read parks
    function all(){
     
        // select all query
        $query = "SELECT * FROM ((TAB_PARCO NATURAL JOIN TAB_INFORMAZIONE) NATURAL JOIN TAB_ZONA) ORDER BY TAB_PARCO.NOME";
     
        // prepare query statement
        $stmt = $this->conn->prepare($query);
     
        // execute query
        $stmt->execute();
     
        return $stmt;
    }

}
?>