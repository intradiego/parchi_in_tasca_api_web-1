<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../config/database.php';
include_once '../objects/park.php';
 
// instantiate database and park object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$park = new park($db);
 
// query products
$stmt = $park->all();
$num = $stmt->rowCount();
 
// check if more than 0 record found
if($num>0){
 
    // products array
    $products_arr=array();
    $products_arr["records"]=array();
 
    // retrieve our table contents
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
 
        $product_item=array(
            "id" => $ID,
            "id_parco" => $ID_PARCO,
            "id_categoria" => $ID_CATEGORIA,
            "nome" => $NOME
        );
 
        array_push($products_arr["records"], $product_item);
    }
 
    echo json_encode($products_arr);
}
 
else{
    echo json_encode(
        array("message" => "No parks found.")
    );
}
?>